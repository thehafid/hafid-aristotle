<?php

namespace App\Policies;

use App\Lecture;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class LecturePolicy
{
    use HandlesAuthorization;

    /**
     * @param  User|null  $user
     * @param  string  $ability
     * @return bool|void
     */
    public function before(?User $user, string $ability)
    {
        if (optional($user)->isAdmin()) {
            return true;
        }
    }

    /**
     * Determine whether the user can view any models.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function viewAny(?User $user)
    {
        return false;
    }

    /**
     * Determine whether the user can view the model.
     *
     * @param  \App\User  $user
     * @param  \App\Lecture  $lecture
     * @return mixed
     */
    public function view(?User $user, Lecture $lecture)
    {
        if ($lecture->course->is_public) {
            return true;
        }

        return optional($user)->id == $lecture->course->user_id;
    }

    /**
     * Determine whether the user can create models.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(?User $user)
    {
        return optional($user)->id > 0;
    }

    /**
     * Determine whether the user can update the model.
     *
     * @param  \App\User  $user
     * @param  \App\Lecture  $lecture
     * @return mixed
     */
    public function update(?User $user, Lecture $lecture)
    {
        return optional($user)->id == $lecture->course->user_id;
    }

    /**
     * Determine whether the user can delete the model.
     *
     * @param  \App\User  $user
     * @param  \App\Lecture  $lecture
     * @return mixed
     */
    public function delete(?User $user, Lecture $lecture)
    {
        return optional($user)->id == $lecture->course->user_id;
    }

    /**
     * Determine whether the user can restore the model.
     *
     * @param  \App\User  $user
     * @param  \App\Lecture  $lecture
     * @return mixed
     */
    public function restore(?User $user, Lecture $lecture)
    {
        return optional($user)->id == $lecture->course->user_id;
    }

    /**
     * Determine whether the user can permanently delete the model.
     *
     * @param  \App\User  $user
     * @param  \App\Lecture  $lecture
     * @return mixed
     */
    public function forceDelete(?User $user, Lecture $lecture)
    {
        return optional($user)->id == $lecture->course->user_id;
    }
}
