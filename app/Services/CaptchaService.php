<?php

namespace App\Services;

use GuzzleHttp\Client;

class CaptchaService
{
    private $http;

    public function __construct(Client $http)
    {
        $this->http = $http;
    }

    public function verify($token)
    {
        if (config('services.captcha.bypass')) {
            return true;
        }

        if (empty($token)) {
            return false;
        }

        $resp = $this->http->post('https://www.google.com/recaptcha/api/siteverify', [
            'form_params' => [
                'secret' => config('services.captcha.secret'),
                'response' => $token
            ]
        ]);

        $body = json_decode((string) $resp->getBody());

        if (
            $body->success != true
            || $body->score < 0.5
            || $body->hostname != config('services.captcha.hostname')
        ) {
            return false;
        }

        return true;
    }
}
