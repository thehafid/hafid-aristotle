<?php

namespace App\Http\Controllers;

use App\Course;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Str;

class CoursesController extends Controller
{
    /**
     * @param  Request  $request
     * @return mixed
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function create(Request $request)
    {
        $this->authorize('create', Course::class);

        return Course::create([
            'user_id' => $this->user()->id,
            'slug' => Str::random(16),
            'title' => 'Untitled',
            'is_public' => false,
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  Course  $course
     * @return mixed
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function show(Course $course)
    {
        $this->authorize('view', $course);

        $data = $course->toArray();
        $data['lectures'] = $course->lectures;

        return $data;
    }

    /**
     * @param  Course  $course
     * @return void
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function update(Course $course)
    {
        $this->authorize('update', $course);

        request()->validate([
            'title' => ['string', 'min:8'],
            'is_public' => ['boolean'],
        ]);

        $course->update(request()->only(['title', 'is_public']));
    }
}
