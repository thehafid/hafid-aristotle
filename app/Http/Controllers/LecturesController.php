<?php

namespace App\Http\Controllers;

use App\Course;
use App\Lecture;
use Illuminate\Http\Request;

class LecturesController extends Controller
{
    /**
     * @param  Course  $course
     * @return mixed
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function create(Course $course)
    {
        $this->authorize('create', Lecture::class);
        $this->authorize('create-lecture', $course);

        $position = 0;

        $finalLecture = $course->lectures()->orderBy('position', 'desc')->first();
        if ($finalLecture) {
            $position = $finalLecture->position + 1;
        }

        return $course->lectures()->create([
            'title' => 'Untitled',
            'content' => 'Lorem ipsum dolor sit amet',
            'position' => $position,
        ]);
    }

    /**
     * @param  Course  $course
     * @param  Lecture  $lecture
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function update(Course $course, Lecture $lecture)
    {
        $this->authorize('update', $lecture);

        request()->validate([
            'title' => ['string'],
            'content' => ['string'],
        ]);

        $lecture->update(request()->only(['title', 'content']));
    }

    public function updatePositions(Course $course)
    {
        $this->authorize('update', $course);

        request()->validate([
            'ids' => ['array', 'required'],
            'ids.*' => ['integer'],
        ]);

        foreach (request()->get('ids') as $i => $id) {
            $course->lectures()->where('id', $id)->update([
                'position' => $i,
            ]);
        }
    }
}
