<?php

namespace App\Http\Controllers\Users;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class MeController extends Controller
{
    /**
     * @param  Request  $request
     * @return \Illuminate\Contracts\Auth\Authenticatable|null
     */
    public function show(Request $request)
    {
        $user = $this->user();
        if ($user === null) {
            abort(204);
        }

        return $user;
    }
}
