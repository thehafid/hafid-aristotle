<?php

namespace App\Http\Controllers\Users;

use App\Http\Controllers\Controller;
use App\Lecture;
use Illuminate\Http\Request;

class LecturesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:sanctum');
    }

    public function show(Lecture $lecture)
    {
        $this->authorize('update', $lecture);

        $payload = $lecture->toArray();
        $payload['course'] = $lecture->course;

        return $payload;
    }
}
