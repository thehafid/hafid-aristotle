<?php

namespace App\Http\Controllers\Users;

use App\Course;
use App\Http\Controllers\Controller;
use App\Http\Resources\CourseCollection;
use Illuminate\Http\Request;

class CoursesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:sanctum');
    }

    /**
     * @return CourseCollection
     */
    public function index()
    {
        $builder = Course
            ::where('user_id', $this->user()->id)
            ->orderBy('updated_at', 'desc')
            ->paginate();

        return new CourseCollection($builder);
    }

    /**
     * @param  Course  $course
     * @return array
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function show(Course $course)
    {
        $this->authorize('update', $course);

        $data = $course->toArray();
        $data['lectures'] = $course->lectures;

        return $data;
    }
}
