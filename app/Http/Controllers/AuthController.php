<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{
    /**
     * @param  Request  $request
     * @return mixed
     */
    public function login(Request $request)
    {
        request()->validate([
            '_grecaptcha' => ['captcha'],
            'email' => ['email', 'required'],
            'password' => ['required'],
        ]);

        if (!Auth::attempt(request()->only(['email', 'password']), true)) {
            abort(403);
        }

        return $this->user();
    }

    /**
     * @param  Request  $request
     * @return void
     */
    public function logout(Request $request)
    {
        Auth::logout();
    }
}
