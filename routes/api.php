<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('/login', 'AuthController@login');
Route::get('/logout', 'AuthController@logout');

Route::post('/courses', 'CoursesController@create');
Route::get('/courses/{course:slug}', 'CoursesController@show');
Route::put('/courses/{course:slug}', 'CoursesController@update');

Route::post('/courses/{course:slug}/lectures', 'LecturesController@create');
Route::put('/courses/{course:slug}/lectures/*/positions', 'LecturesController@updatePositions');
Route::put('/courses/{course:slug}/lectures/{lecture}', 'LecturesController@update');

Route::get('/users/me', 'Users\MeController@show');
Route::get('/users/me/courses', 'Users\CoursesController@index');
Route::get('/users/me/courses/{course:slug}', 'Users\CoursesController@show');
Route::get('/users/me/lectures/{lecture}', 'Users\LecturesController@show');
