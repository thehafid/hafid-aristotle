<?php

namespace Tests\Feature\Users;

use App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Laravel\Sanctum\Sanctum;
use Tests\TestCase;

class MeControllerTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function user_can_receive_own_info()
    {
        $user = factory(User::class)->create();

        Sanctum::actingAs($user);

        $response = $this->get('/api/users/me');

        $response
            ->assertOk()
            ->assertJsonStructure([
                'id',
                'name',
                'email',
                'email_verified_at',
                'created_at',
                'updated_at',
            ]);
    }

    /** @test */
    public function guess_cant_receive_own_info()
    {
        $response = $this->get('/api/users/me');

        $response
            ->assertNoContent()
            ->assertDontSee('email');
    }
}
