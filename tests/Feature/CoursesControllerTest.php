<?php

namespace Tests\Feature;

use App\Course;
use App\Lecture;
use App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Str;
use Laravel\Sanctum\Sanctum;
use Tests\TestCase;

class CoursesControllerTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    function guest_can_view_course()
    {
        $course = factory(Course::class)->create();
        $course->lectures()->save(factory(Lecture::class)->make());

        $response = $this->get('/api/courses/'.$course->slug);

        $response
            ->assertOk()
            ->assertJsonStructure([
                'id',
                'user_id',
                'slug',
                'title',
                'is_public',
                'created_at',
                'updated_at',
                'lectures' => [
                    '*' => [
                        'id',
                        'course_id',
                        'title',
                        'content',
                        'position',
                        'created_at',
                        'updated_at',
                    ],
                ],
            ]);
    }

    /** @test */
    function guest_cant_view_private_course()
    {
        $course = factory(Course::class)->create([
            'is_public' => false,
        ]);

        $response = $this->get(sprintf('/api/courses/%s', $course->slug));

        $response
            ->assertForbidden()
            ->assertDontSee($course->title);
    }

    /** @test */
    function admin_can_view_private_course()
    {
        $user = factory(User::class)->create([
            'email' => 'zenkilies@gmail.com',
        ]);

        $course = factory(Course::class)->create([
            'is_public' => false,
        ]);

        Sanctum::actingAs($user);

        $response = $this->get(sprintf('/api/courses/%s', $course->slug));

        $response
            ->assertOk()
            ->assertSee($course->title);
    }

    /** @test */
    function user_can_create_course()
    {
        $user = factory(User::class)->create();

        Sanctum::actingAs($user);

        $response = $this->postJson('/api/courses');

        $response
            ->assertCreated()
            ->assertJsonStructure([
                'id',
                'user_id',
                'slug',
                'title',
                'is_public',
                'created_at',
                'updated_at',
            ]);
    }

    /** @test */
    function guest_cant_create_course()
    {
        $response = $this->postJson('/api/courses');
        $response->assertForbidden();
    }

    /** @test */
    function user_can_update_course()
    {
        $user = factory(User::class)->create();

        $course = factory(Course::class)->create([
            'user_id' => $user->id,
            'is_public' => false,
        ]);

        Sanctum::actingAs($user);

        $updatedTitle = Str::random();

        $response = $this->putJson(sprintf('/api/courses/%s', $course->slug), [
            'title' => $updatedTitle,
            'is_public' => true,
        ]);

        $response->assertOk();

        $this->assertDatabaseHas('courses', [
            'id' => $course->id,
            'title' => $updatedTitle,
            'is_public' => true,
        ]);
    }

    /** @test */
    public function user_cant_update_unowned_course()
    {
        $user = factory(User::class)->create();

        $course = factory(Course::class)->create([
            'is_public' => false,
        ]);

        Sanctum::actingAs($user);

        $updatedTitle = Str::random();

        $response = $this->putJson(sprintf('/api/courses/%s', $course->slug), [
            'title' => $updatedTitle,
            'is_public' => true,
        ]);

        $response->assertForbidden();

        $this->assertDatabaseMissing('courses', [
            'id' => $course->id,
            'title' => $updatedTitle,
            'is_public' => true,
        ]);
    }
}
