<?php

namespace Tests\Feature;

use App\Course;
use App\Lecture;
use App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Str;
use Laravel\Sanctum\Sanctum;
use Tests\TestCase;

class LecturesControllerTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    function user_can_create_lecture()
    {
        $course = factory(Course::class)->create();

        Sanctum::actingAs($course->user);

        $response = $this->postJson(sprintf('/api/courses/%s/lectures', $course->slug));

        $response
            ->assertCreated()
            ->assertJsonStructure([
                'id',
                'course_id',
                'title',
                'content',
                'position',
                'created_at',
                'updated_at',
            ]);
    }

    /** @test */
    function user_can_create_lecture_with_increased_position()
    {
        $randomPosition = rand();

        $course = factory(Course::class)->create();
        $course->lectures()->save(factory(Lecture::class)->make(['position' => $randomPosition]));

        Sanctum::actingAs($course->user);

        $response = $this->postJson(sprintf('/api/courses/%s/lectures', $course->slug));

        $response
            ->assertCreated()
            ->assertJson([
                'position' => $randomPosition + 1,
            ]);
    }

    /** @test */
    function user_cant_create_lecture_with_unowned_course()
    {
        $user = factory(User::class)->create();

        $course = factory(Course::class)->create();

        Sanctum::actingAs($user);

        $response = $this->postJson(sprintf('/api/courses/%s/lectures', $course->slug));

        $response->assertForbidden();

        $this->assertDatabaseMissing('lectures', [
            'course_id' => $course->id,
        ]);
    }

    /** @test */
    function user_can_update_lecture()
    {
        $course = factory(Course::class)->create();
        $lecture = factory(Lecture::class)->create(['course_id' => $course->id]);

        Sanctum::actingAs($course->user);

        $randomTitle = Str::random();
        $randomContent = Str::random();

        $response = $this->putJson(sprintf('/api/courses/%s/lectures/%d', $course->slug, $lecture->id), [
            'title' => $randomTitle,
            'content' => $randomContent,
        ]);

        $response->assertOk();

        $this->assertDatabaseHas('lectures', [
            'id' => $lecture->id,
            'title' => $randomTitle,
            'content' => $randomContent,
        ]);
    }

    /** @test */
    function user_cant_update_unowned_lecture()
    {
        $user = factory(User::class)->create();

        $course = factory(Course::class)->create();
        $lecture = factory(Lecture::class)->create(['course_id' => $course->id]);

        Sanctum::actingAs($user);

        $randomTitle = Str::random();
        $randomContent = Str::random();

        $response = $this->putJson(sprintf('/api/courses/%s/lectures/%d', $course->slug, $lecture->id), [
            'title' => $randomTitle,
            'content' => $randomContent,
        ]);

        $response->assertForbidden();

        $this->assertDatabaseMissing('lectures', [
            'id' => $lecture->id,
            'title' => $randomTitle,
            'content' => $randomContent,
        ]);
    }

    /** @test */
    function user_can_update_positions()
    {
        $positions = [7, 6, 3, 4, 1, 2, 5, 8, 0, 9];

        $course = factory(Course::class)->create();
        $lectures = factory(Lecture::class, count($positions))->create(['course_id' => $course]);

        Sanctum::actingAs($course->user);

        $ids = [];
        foreach ($positions as $position) {
            $ids[] = $lectures[$position]->id;
        }

        $response = $this->putJson(sprintf('/api/courses/%s/lectures/*/positions', $course->slug), [
            'ids' => $ids,
        ]);

        $response->assertOk();

        foreach ($course->lectures as $i => $lecture) {
            $this->assertEquals($ids[$i], $lecture->id);
        }
    }
}
