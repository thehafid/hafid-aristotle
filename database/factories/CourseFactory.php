<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Course;
use Faker\Generator as Faker;

$factory->define(Course::class, function (Faker $faker) {
    return [
        'user_id' => factory(\App\User::class),
        'slug' => $faker->uuid,
        'title' => $faker->sentence,
        'is_public' => true,
    ];
});
