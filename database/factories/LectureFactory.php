<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Lecture;
use Faker\Generator as Faker;

$factory->define(Lecture::class, function (Faker $faker) {
    return [
        'course_id' => factory(\App\Course::class),
        'title' => $faker->sentence(),
        'content' => implode(' ', $faker->sentences(6)),
        'position' => 0,
    ];
});
